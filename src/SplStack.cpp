//******************************************************************************
// Program: Simulation Programming Compiler (spc)
// Author: Matthew Nash
// Contributors:
//
// File: SplStack.h
// Description: Stack to make postfix intermediate code
// License: GPLv3
//
// This program is free software: you can redistribute it and/or modify it 
// under the terms of the GNU General Public License as published by the Free 
// Software Foundation, either version 3 of the License, or (at your option) 
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for 
// more details.
//
// You should have received a copy of the GNU General Public License along with 
// this program. If not, see https://www.gnu.org/licenses/. 
//******************************************************************************

#include "SplStack.h"

namespace spc
{

SplStack::SplStack() :
  _height(0),
  _top(NULL)
{

}

SplStack::~SplStack()
{
  while(_top != NULL)
  {
    SplNode* toDelete = _top;
    _top = _top->right;
    _top->left = NULL;
    delete toDelete; 
  }
}

SPL_INTEGER SplStack::push(SplToken& pushInput)
{
  SplNode* toPush = new SplNode(pushInput);
  if (_top == NULL)
  {
    _top = toPush;
  }
  else
  {
    _top->left = toPush;
    toPush->right = _top;
    toPush->left = NULL;
    _top = toPush;
  }
  _height = _height+1;
}

SplToken SplStack::pop()
{
  SplToken toReturn; 

  if (_top == NULL)
  {
    toReturn.tokenType = SPL_TOKEN_UNKNOWN;
    toReturn.baseType = SPL_BASE_CHARACTERS;
    toReturn.name = "WARNING - STACK EMPTY";
    toReturn.characters = "WARNING - STACK EMPTY";
    return toReturn;
  }

  toReturn = _top->token;
  
  SplNode* toDelete = _top;
  _top = _top->right;
  if (_top != NULL)
  {
    _top->left = NULL;
  }
  delete toDelete;

  _height = _height - 1;

  return toReturn;
}

SplToken SplStack::pop
(
  //output
  SPL_INTEGER& newHeight
)
{
  SplToken toReturn; 

  if (_top == NULL)
  {
    toReturn.tokenType = SPL_TOKEN_UNKNOWN;
    toReturn.baseType = SPL_BASE_CHARACTERS;
    toReturn.name = "WARNING - STACK EMPTY";
    toReturn.characters = "WARNING - STACK EMPTY";
    return toReturn;
  }

  toReturn = _top->token;
  
  SplNode* toDelete = _top;
  _top = _top->right;
  if (_top != NULL)
  {
    _top->left = NULL;
  }
  delete toDelete;

  _height = _height - 1;
  newHeight = _height;

  return toReturn;
}

SplToken SplStack::peek()
{
  SplToken toReturn; 

  if (_top == NULL)
  {
    toReturn.tokenType = SPL_TOKEN_UNKNOWN;
    toReturn.baseType = SPL_BASE_CHARACTERS;
    toReturn.name = "WARNING - STACK EMPTY";
    toReturn.characters = "WARNING - STACK EMPTY";
    return toReturn;
  }

  toReturn = _top->token;
  return toReturn;
}

SplToken SplStack::peek
(
  //output
  SPL_INTEGER& heightOutput
)
{
  SplToken toReturn; 

  if (_top == NULL)
  {
    toReturn.tokenType = SPL_TOKEN_UNKNOWN;
    toReturn.baseType = SPL_BASE_CHARACTERS;
    toReturn.name = "WARNING - STACK EMPTY";
    toReturn.characters = "WARNING - STACK EMPTY";
    return toReturn;
  }

  toReturn = _top->token;
  heightOutput = _height;
  return toReturn;
}

SPL_INTEGER SplStack::getHeight()
{
  return _height;
}


}