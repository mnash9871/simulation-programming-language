//******************************************************************************
// Program: Simulation Programming Compiler (spc)
// Author: Matthew Nash
// Contributors:
//
// File: SplGlobal.h
// Description: Items and functions needed by all parts of spc
// License: GPLv3
//
// This program is free software: you can redistribute it and/or modify it 
// under the terms of the GNU General Public License as published by the Free 
// Software Foundation, either version 3 of the License, or (at your option) 
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for 
// more details.
//
// You should have received a copy of the GNU General Public License along with 
// this program. If not, see https://www.gnu.org/licenses/. 
//******************************************************************************

#include "SplGlobals.h"

namespace spc 
{

extern SplTree* SPL_WHITESPACE_DICTIONARY = NULL;
extern SplTree* SPL_OPERATOR_DICTIONARY = NULL;
extern SplTree* SPL_KEYWORD_DICTIONARY = NULL;
extern SplTree* SPL_SYMBOL_DICTIONARY = NULL;
extern SplTree* SPL_INTEGERS_DICTIONARY = NULL;
extern SplToken* SPL_LEXEMES_LIST_BEGIN = NULL;
extern SplToken* SPL_LEXEMES_LIST_END = NULL;
extern SplStack* SPL_PARSER_STACK = NULL;
extern SplStack* SPL_PARSED_LIST_BEGIN = NULL;

void loadWhitespaceDictionary()
{

}

void loadOperatorDictionary()
{
  
}

void loadIntegersDictionary()
{

}

void loadKeywordDictionary()
{

}

bool checkWhitespaceDictionary(SplToken& toCheck)
{
  if (SPL_WHITESPACE_DICTIONARY->contains(toCheck.name))
  {
    return true;
  }
  else
  {
    return false;
  }
}

bool checkOperatorDictionary(SplToken& toCheck)
{
  if (SPL_OPERATOR_DICTIONARY->contains(toCheck.name))
  {
    return true;
  }
  else
  {
    return false;
  }
}

bool checkIntegersDictionary(SplToken& toCheck)
{
  if (SPL_INTEGERS_DICTIONARY->contains(toCheck.name))
  {
    return true;
  }
  else
  {
    return false;
  }
}

bool checkKeywordDictionary(SplToken& toCheck)
{
  if (SPL_KEYWORD_DICTIONARY->contains(toCheck.name))
  {
    return true;
  }
  else
  {
    return false;
  }
}

bool checkSymbolDictionary(SplToken& toCheck)
{
  if (SPL_SYMBOL_DICTIONARY->contains(toCheck.name))
  {
    return true;
  }
  else
  {
    return false;
  }
}

void initialize()
{
  if (SPL_WHITESPACE_DICTIONARY == NULL)
  {
    SPL_WHITESPACE_DICTIONARY = new SplTree();
    loadWhitespaceDictionary();
  }
  if (SPL_OPERATOR_DICTIONARY == NULL)
  {
    SPL_OPERATOR_DICTIONARY = new SplTree();
    loadOperatorDictionary();
  }
  if (SPL_KEYWORD_DICTIONARY == NULL)
  {
    SPL_KEYWORD_DICTIONARY = new SplTree();
    loadKeywordDictionary();
  }
  if (SPL_INTEGERS_DICTIONARY == NULL)
  {
    SPL_INTEGERS_DICTIONARY = new SplTree();
    loadIntegersDictionary();
  }
  if (SPL_SYMBOL_DICTIONARY == NULL)
  {
    SPL_SYMBOL_DICTIONARY = new SplTree();
  }
  if (SPL_PARSER_STACK == NULL)
  {
    SPL_PARSER_STACK = new SplStack();
  }
  if (SPL_LEXEMES_LIST_BEGIN == NULL)
  {
    SPL_LEXEMES_LIST_BEGIN = new SplToken();
    SPL_LEXEMES_LIST_END = SPL_LEXEMES_LIST_BEGIN;
  }
}

void close()
{
  if (SPL_WHITESPACE_DICTIONARY != NULL)
  {
    delete SPL_WHITESPACE_DICTIONARY;
  }
  if (SPL_OPERATOR_DICTIONARY != NULL)
  {
    delete SPL_OPERATOR_DICTIONARY;
  }
  if (SPL_KEYWORD_DICTIONARY != NULL)
  {
    delete SPL_KEYWORD_DICTIONARY;
  }
  if (SPL_INTEGERS_DICTIONARY != NULL)
  {
    delete SPL_INTEGERS_DICTIONARY;
  }
  if (SPL_SYMBOL_DICTIONARY != NULL)
  {
    delete SPL_SYMBOL_DICTIONARY;
  }
  if (SPL_LEXEMES_LIST_BEGIN != NULL)
  {
    while (SPL_LEXEMES_LIST_BEGIN != NULL)
    {
      SplToken* toDelete = SPL_LEXEMES_LIST_BEGIN;
      SPL_LEXEMES_LIST_BEGIN = SPL_LEXEMES_LIST_BEGIN->getNext();
      toDelete->next = NULL;
      delete toDelete;
    }
    SPL_LEXEMES_LIST_BEGIN = NULL;
    SPL_LEXEMES_LIST_END = NULL;
  }
}

}