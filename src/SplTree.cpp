//******************************************************************************
// Program: Simulation Programming Compiler (spc)
// Author: Matthew Nash
// Contributors:
//
// File: SplTree.cpp
// Description: Collective data structure used in spc
// License: GPLv3
//
// This program is free software: you can redistribute it and/or modify it 
// under the terms of the GNU General Public License as published by the Free 
// Software Foundation, either version 3 of the License, or (at your option) 
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for 
// more details.
//
// You should have received a copy of the GNU General Public License along with 
// this program. If not, see https://www.gnu.org/licenses/. 
//******************************************************************************


#include "SplTree.h"
#include <iostream>

namespace spc
{

SplTree::SplTree() :
  _top(NULL)
{
}

SplTree::~SplTree()
{

}

void SplTree::insert(SplToken newToken)
{
  place(_top, &newToken);
}

SplToken SplTree::getToken(SPL_CHARACTERS tokenName)
{
  SplNode* found = find(_top, tokenName);
  if (found == NULL)
  {
    return SplToken();
  }
  else
  {
    return found->token;
  }
}

bool SplTree::contains(SPL_CHARACTERS tokenName)
{
  SplNode* found = find(_top, tokenName);
  if (found == NULL)
  {
    return false;
  }
  else
  {
    return true;
  }
}

SplNode* SplTree::find(SplNode* top, SPL_CHARACTERS tokenName)
{
  if (top == NULL)
  {
    return top;
  }

  if (tokenName < top->token.name)
  {
    return find(top->left, tokenName);
  }
  else if (tokenName > top->token.name)
  {
    return find(top->right, tokenName);
  }
  else
  {
    return top;
  }
}

bool SplTree::isEmpty()
{
  if (_top == NULL)
  {
    return true;
  }
  else 
  {
    return false;
  }
}

void SplTree::remove(SPL_CHARACTERS tokenName)
{
  _top = removeNode(_top, tokenName);
}

SPL_INTEGER SplTree::getHeight(SplNode* n)
{
  if (n == NULL)
  {
    return 0;
  }
  else
  {
    return n->height;
  }
}

SPL_INTEGER SplTree::maxHeight(SplNode *a, SplNode *b)
{
  SPL_INTEGER aHeight = getHeight(a);
  SPL_INTEGER bHeight = getHeight(b);

  return (aHeight > bHeight) ? aHeight : bHeight;
}

SPL_INTEGER SplTree::compare(SplNode* a, SplNode* b)
{
  assert(a != NULL);
  assert(b != NULL);

  if (a->token.name < b->token.name)
  {
    return -1;
  }
  else if (a->token.name > b->token.name)
  {
    return 1;
  }
  else
  {
    return 0;
  }

}

SplNode *SplTree::rotateRight(SplNode* y)
{
  assert(y != NULL);

  SplNode* x = y->left;
  SplNode* t2 = x->right;

  //rotate
  x->right = y;
  y->left = t2;

  y->height = maxHeight(y->left, y->right) + 1;
  x->height = maxHeight(x->left, x->right) + 1;
  
  return x;
}

SplNode *SplTree::rotateLeft(SplNode *x)
{
  assert(x != NULL);

  SplNode* y = x->right;
  SplNode* t2 = y->left;

  //rotate
  x->right = t2;
  y->left = x;

  y->height = maxHeight(y->left, y->right) + 1;
  x->height = maxHeight(x->left, x->right) + 1;
  
  return y;
}

SPL_INTEGER SplTree::getBalance(SplNode* n)
{
  if (n == NULL)
  {
    return 0;
  }
  return getHeight(n->left) - getHeight(n->right);
}

SplNode* SplTree::place(SplNode* node, SplToken* token)
{
  if (node == NULL)
  {
    node = new SplNode(*token);
    return node;
  }

  if (token->name < node->token.name)
  {
    node->left = place(node->left, token);
  }
  else if (token->name > node->token.name)
  {
    node->right = place(node->right, token);
  }
  else
  {
    node->token = *token;
    return node;
  }

  node->height = maxHeight(node->left, node->right) + 1;

  SPL_INTEGER balance = getBalance(node);

  // left left case
  if 
  (
    (balance > 1) && 
    (token->name < node->left->token.name)
  )
  {
    return rotateRight(node);
  }

  // right right case
  if 
  (
    (balance < -1) && 
    (token->name > node->right->token.name)
  )
  {
    return rotateLeft(node);
  }

  // left right case
  if 
  (
    (balance > 1) && 
    (token->name > node->left->token.name)
  )
  {
    node->left = rotateLeft(node->left);
    return rotateRight(node);
  }

  // right left case
  if 
  (
    (balance < -1) && 
    (token->name < node->right->token.name)
  )
  {
    node->right = rotateRight(node->right);
    return rotateLeft(node);
  }

  return node;

}

SplNode* SplTree::removeNode(SplNode* top, SPL_CHARACTERS tokenName)
{
  if (top == NULL)
  {
    return top;
  }

  if (tokenName < top->token.name)
  {
    top->left = removeNode(top->left, tokenName);
  }
  else if (tokenName > top->token.name)
  {
    top->right = removeNode(top->right, tokenName);
  }
  else
  {
    SPL_INTEGER leftHeight = getHeight(top->left);
    SPL_INTEGER rightHeight = getHeight(top->right);

    SplNode* tempNode;
    if // both are null, and there are no children
    (
      (leftHeight == 0) &&
      (rightHeight == 0)
    )
    {
      tempNode = top;
      top = NULL;
      delete tempNode;
    }
    else if (leftHeight == 0) // left is null, but right isn't
    {
      tempNode = top->right;
      *top = *tempNode;
      delete tempNode;
    }
    else if (rightHeight == 0) // right is null, but left isn't
    {
      tempNode = top->left;
      *top = *tempNode;
      delete tempNode;
    }
    else // both sides have chillens
    {
      // get smallest in the right subtree
      tempNode = top->right;
      while (tempNode->left != NULL)
      {
        tempNode = tempNode->left;
      }
      
      top->token = tempNode->token;

      top->right = removeNode(top->right, tempNode->token.name);
    }
  }

  if (top == NULL)
  {
    return NULL;
  }

  top->height = maxHeight(top->left, top->right) + 1;

  SPL_INTEGER balance = getBalance(top);

  // left left case
  if 
  (
    (balance > 1) && 
    (getBalance(top->left) >= 0)
  )
  {
    return rotateRight(top);
  }

  // right right case
  if 
  (
    (balance < -1) && 
    (getBalance(top->right) <= 0)
  )
  {
    return rotateLeft(top);
  }

  // left right case
  if 
  (
    (balance > 1) && 
    (getBalance(top->left) < 0)
  )
  {
    top->left = rotateLeft(top->left);
    return rotateRight(top);
  }

  // right left case
  if 
  (
    (balance < -1) && 
    (getBalance(top->right) > 0)
  )
  {
    top->right = rotateRight(top->right);
    return rotateLeft(top);
  }

  return top;
}

void SplTree::printTree(SplNode* top, SPL_INTEGER level)
{
  for (SPL_INTEGER i=0; i<level; ++i)
  {
    std::cout << "  ";
  }
  if (top != NULL)
  {
    std::cout << top->token.name << std::endl;
    printTree(top->left, level+1);
    printTree(top->right, level+1);
  }
  else
  {
    std::cout << "EMPTY-LEAF" << std::endl;
  }
}

}