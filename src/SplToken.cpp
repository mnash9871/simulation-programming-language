//******************************************************************************
// Program: Simulation Programming Compiler (spc)
// Author: Matthew Nash
// Contributors:
//
// File: SplToken.h
// Description: Token data structure used in spc
// License: GPLv3
//
// This program is free software: you can redistribute it and/or modify it 
// under the terms of the GNU General Public License as published by the Free 
// Software Foundation, either version 3 of the License, or (at your option) 
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for 
// more details.
//
// You should have received a copy of the GNU General Public License along with 
// this program. If not, see https://www.gnu.org/licenses/. 
//******************************************************************************

#include "SplToken.h"

namespace spc
{

SplToken::SplToken() :
  next(NULL)
{
  initialize();
}

SplToken::SplToken(SPL_CHARACTERS newName) :
  next(NULL)
{
  initialize();
  name = newName;
}

SplToken::SplToken(SPL_CHARACTERS newName, SPL_INTEGER newValue) :
  next(NULL)
{
  initialize();
  name = newName;
  baseType = SPL_BASE_INTEGER;
  tokenType = SPL_TOKEN_UNKNOWN;
  integer = newValue;
}

SplToken::SplToken(SPL_CHARACTERS newName, SPL_FLOAT newValue) :
  next(NULL)
{
  initialize();
  name = newName;
  baseType = SPL_BASE_FLOAT;
  tokenType = SPL_TOKEN_UNKNOWN;
  floating = newValue;
}

SplToken::SplToken(SPL_CHARACTERS newName, SPL_CHARACTERS newValue) :
  next(NULL)
{
  initialize();
  name = newName;
  baseType = SPL_BASE_CHARACTERS;
  tokenType = SPL_TOKEN_UNKNOWN;
  characters = newValue;
}

SplToken::SplToken(SplToken& other)
{
  copy(other);
}

SplToken::~SplToken()
{
  //nothing to do here, 
  // user is responsible for destroying their own pointer data
}

void SplToken::operator=(const SplToken& other)
{
  copy(other);
}

//getters
SPL_BASE_TYPES SplToken::getBaseType()
{
  return baseType;
}

SPL_TOKEN_TYPES SplToken::getTokenType()
{
  return tokenType;
}

SPL_POINTER SplToken::getPointer()
{
  assert(baseType == SPL_BASE_POINTER);
  return pointer;
}

SPL_INTEGER SplToken::getInteger()
{
  assert(baseType == SPL_BASE_INTEGER);
  return integer;
}

SPL_FLOAT SplToken::getFloat()
{
  assert(baseType == SPL_BASE_FLOAT);
  return floating;
}

SPL_CHARACTERS SplToken::getCharacters()
{
  assert(baseType == SPL_BASE_CHARACTERS);
  return characters;
}

SplToken* SplToken::getNext()
{
  return next;
}

//setters
void SplToken::setBaseType(SPL_BASE_TYPES newType)
{
  baseType = newType;
  clean();
}

void SplToken::setTokenType(SPL_TOKEN_TYPES newType)
{
  tokenType = newType;
  clean();
}

void SplToken::setPointer(SPL_POINTER newValue)
{
  assert(baseType == SPL_BASE_POINTER);
  pointer = newValue;
}

void SplToken::setInteger(SPL_INTEGER newValue)
{
  assert(baseType == SPL_BASE_INTEGER);
  integer = newValue;
}

void SplToken::setFloat(SPL_FLOAT newValue)
{
  assert(baseType == SPL_BASE_FLOAT);
  floating = newValue;
}

void SplToken::setCharacters(SPL_CHARACTERS newValue)
{
  assert(baseType == SPL_BASE_CHARACTERS);
  characters = newValue;
}

void SplToken::clear()
{
  pointer = NULL;
  integer = 0;
  floating = 0.0;
  characters = "";
}

void SplToken::clean()
{
  if (baseType != SPL_BASE_POINTER)
  {
    pointer = NULL;
  }
  if (baseType != SPL_BASE_INTEGER)
  {
    integer = 0;
  }
  if (baseType != SPL_BASE_FLOAT)
  {
    floating = 0.0;
  }
  if (baseType != SPL_BASE_CHARACTERS)
  {
    characters = "";
  }
}

void SplToken::nullify()
{
  baseType = SPL_BASE_POINTER;
  tokenType = SPL_TOKEN_UNKNOWN;
  pointer = NULL;
  integer = 0;
  floating = 0.0;
  characters = "";
}

void SplToken::initialize()
{
  name = "";
  baseType = SPL_BASE_POINTER;
  tokenType = SPL_TOKEN_UNKNOWN;
  pointer = NULL;
  integer = 0;
  floating = 0.0;
  characters = "";
  next = NULL;
}

void SplToken::copy(const SplToken& other)
{
  name = other.name;
  baseType = other.baseType;
  tokenType = other.tokenType;
  pointer = other.pointer;
  integer = other.integer;
  floating = other.floating;
  characters = other.characters;
  next = other.next;
}

}


