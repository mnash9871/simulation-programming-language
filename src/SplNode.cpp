//******************************************************************************
// Program: Simulation Programming Compiler (spc)
// Author: Matthew Nash
// Contributors:
//
// File: SplNode.cpp
// Description: Node data structure used in SplTree spc
// License: GPLv3
//
// This program is free software: you can redistribute it and/or modify it 
// under the terms of the GNU General Public License as published by the Free 
// Software Foundation, either version 3 of the License, or (at your option) 
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for 
// more details.
//
// You should have received a copy of the GNU General Public License along with 
// this program. If not, see https://www.gnu.org/licenses/. 
//******************************************************************************

#include "SplNode.h"

namespace spc
{

SplNode::SplNode() :
  token(),
  left(NULL),
  right(NULL),
  height(1)
{
  
}

SplNode::SplNode(SplToken& otherToken) :
  token(otherToken),
  left(NULL),
  right(NULL),
  height(1)
{
  
}

SplNode::SplNode(SplNode& other) :
  token(other.token),
  left(other.left),
  right(other.right),
  height(other.height)
{

}

void SplNode::operator=(const SplNode& other)
{
  token = other.token;
  left = other.left;
  right = other.right;
  height = other.height;
}

}
