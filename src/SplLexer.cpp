//******************************************************************************
// Program: Simulation Programming Compiler (spc)
// Author: Matthew Nash
// Contributors:
//
// File: SplLexer.cpp
// Description: Lexical analyzer for spc
// License: GPLv3
//
// This program is free software: you can redistribute it and/or modify it 
// under the terms of the GNU General Public License as published by the Free 
// Software Foundation, either version 3 of the License, or (at your option) 
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for 
// more details.
//
// You should have received a copy of the GNU General Public License along with 
// this program. If not, see https://www.gnu.org/licenses/. 
//******************************************************************************

#include "SplLexer.h"

namespace spc
{

extern SplToken SPL_LEXEME;

void trimLexeme()
{
  // case: we have trimmed a keyword
  SPL_LEXEME.name = SPL_LEXEME.characters; 
  if (checkKeywordDictionary(SPL_LEXEME))
  {
    SPL_LEXEME.tokenType = SPL_TOKEN_KEYWORD;
    if (SPL_LEXEME.name == "INTEGER")
    {
      SPL_LEXEME.baseType = SPL_BASE_INTEGER;
    }
    else if (SPL_LEXEME.name == "FLOAT")
    {
      SPL_LEXEME.baseType = SPL_BASE_FLOAT;
    }
    else if (SPL_LEXEME.name == "POINTER")
    {
      SPL_LEXEME.baseType = SPL_BASE_POINTER;
    }
    else
    {
      SPL_LEXEME.baseType = SPL_BASE_CHARACTERS;
    } 
  }
  // case: we've trimmed a symbol name
  else
  {
    SPL_LEXEME.tokenType = SPL_TOKEN_SYMBOL;
    SPL_LEXEME.baseType = SPL_BASE_POINTER;
  }
  
  //add this lexeme to the list
  SplToken* newToken = new SplToken(SPL_LEXEME);
  SPL_LEXEMES_LIST_END->next = newToken;
  SPL_LEXEMES_LIST_END = SPL_LEXEMES_LIST_END->next;

  //zero out our lexeme
  SPL_LEXEME = SplToken();
}


void lex
(
  //inputs
  SPL_CHARACTERS fileName
)
{

  std::ifstream sourceFile(fileName);

  assert(sourceFile.is_open());
  
  char character;
  bool buildingLexeme = false;
  bool buildingComment = false;
  bool buildingStringLiteral = false;
  while (sourceFile.good()) 
  {
    //read in our character
    sourceFile.get(character);
    SPL_CHARACTERS characterAsCharacters(1, character);

    //check to see if we're building a comment
    if (buildingComment)
    {
      if 
      (
        SPL_WHITESPACE_DICTIONARY->getToken("COMMENT_CLOSE").characters ==
          characterAsCharacters
      )
      {
        buildingComment = false;
      }
      continue;
    }

    // check to see if we're building a string
    if (buildingStringLiteral)
    {
      if (characterAsCharacters == "\"")
      {
        SplToken* newToken = new SplToken(SPL_LEXEME);
        SPL_LEXEMES_LIST_END->next = newToken;
        SPL_LEXEMES_LIST_END = SPL_LEXEMES_LIST_END->next;
        buildingStringLiteral = false;
      }
      else
      {
        SPL_LEXEME.characters = SPL_LEXEME.characters + characterAsCharacters;
        SPL_LEXEME.name = SPL_LEXEME.characters;
      }
      continue;
    }

    //check to see if we're building anything at the moment
    buildingLexeme = false;
    if 
    (
      (SPL_LEXEME.getTokenType() == SPL_TOKEN_UNKNOWN) &&
      (SPL_LEXEME.getBaseType() == SPL_BASE_POINTER) &&
      (SPL_LEXEME.getPointer() == NULL)
    )
    {
      buildingLexeme = false;
    }
    else
    {
      buildingLexeme = true;
    }

    //create a temporary lexeme to use in comparisons
    SplToken lexemeCharacter(characterAsCharacters, characterAsCharacters);
    
    //check whitespace first
    if (checkWhitespaceDictionary(lexemeCharacter))
    {
      //if this is an open comment
      if 
      (
        SPL_WHITESPACE_DICTIONARY->getToken("COMMENT_OPEN").characters == characterAsCharacters 
      )
      {
        buildingComment = true;
      }

      //trim current built lexeme if needed
      if (buildingLexeme) trimLexeme();

      //ignore all whitespace
      continue;
    }

    //next, check if it's an operator
    if (checkOperatorDictionary(lexemeCharacter))
    {
      //see if we're making a floating literal
      if 
      (
        buildingLexeme && 
        (SPL_LEXEME.getBaseType() == SPL_BASE_INTEGER)
      )
      {
        //make the lexeme's literal type a float
        SPL_LEXEME.setBaseType(SPL_BASE_FLOAT);
        SPL_LEXEME.floating = std::stod(SPL_LEXEME.characters);
      }
      //we were already building a float and we have a syntax error
      else if 
      (
        buildingLexeme && 
        (SPL_LEXEME.getBaseType() == SPL_BASE_FLOAT)
      )
      {
        //TODO: add syntax error
      }
      //trim the symbol or keyword lexeme being built if needed
      else if (buildingLexeme)
      {
        trimLexeme();
      }
      
      //it's just a simple operator
      else
      {
        //add this lexeme to the list
        SplToken* newToken = new SplToken(lexemeCharacter.characters, lexemeCharacter.characters);
        newToken->tokenType = SPL_TOKEN_OPERATOR;
        SPL_LEXEMES_LIST_END->next = newToken;
        SPL_LEXEMES_LIST_END = SPL_LEXEMES_LIST_END->next;
      }
      continue;
    }    

    //if we're not building anything then we need to 
    if (!buildingLexeme)
    {
      //are we building a number?
      if (checkIntegersDictionary(lexemeCharacter))
      {
        SPL_LEXEME = SplToken(lexemeCharacter.name, std::stoll(lexemeCharacter.name));
        SPL_LEXEME.characters = lexemeCharacter.name;
      }
      //or is it a string?
      else if (lexemeCharacter.name == "\"")
      {
        SPL_LEXEME = SplToken("", "");
        buildingStringLiteral = true;
      }
      //we're making a symbol or keyword, we don't know yet
      else
      {
        SPL_LEXEME = SplToken(lexemeCharacter.name, lexemeCharacter.name);
        SPL_LEXEME.tokenType = SPL_TOKEN_SYMBOL;
      }

      continue;
    }

    //if we are building something then add it to the list
    SPL_LEXEME.characters = SPL_LEXEME.characters + lexemeCharacter.name;
    SPL_LEXEME.name = SPL_LEXEME.characters;
    //update our integer value
    if (SPL_LEXEME.getBaseType() == SPL_BASE_INTEGER)
    {
      SPL_LEXEME.integer = std::stoll(SPL_LEXEME.characters);
    }
    //update our float value
    else if (SPL_LEXEME.getBaseType() == SPL_BASE_FLOAT)
    {
      SPL_LEXEME.floating = std::stod(SPL_LEXEME.characters);
    }
    //see if we need to turn our string into a keyword
    else if (SPL_LEXEME.getBaseType() == SPL_TOKEN_SYMBOL)
    {
      if (!checkKeywordDictionary(SPL_LEXEME)) continue;

      SPL_LEXEME.tokenType = SPL_TOKEN_KEYWORD;

      //add this lexeme to the list
      SplToken* newToken = new SplToken(SPL_LEXEME);
      SPL_LEXEMES_LIST_END->next = newToken;
      SPL_LEXEMES_LIST_END = SPL_LEXEMES_LIST_END->next;

      //zero out our lexeme
      SPL_LEXEME = SplToken();
    }

  }

  sourceFile.close();

}



void printLexemes()
{
  SplToken* node = SPL_LEXEMES_LIST_BEGIN;
  SPL_INTEGER lexemeCount = 1;
  while (node != NULL)
  {
    std::cout << "Lexeme [" << lexemeCount << "]: " << node->name << "=";
    if (node->getBaseType() == SPL_BASE_INTEGER)
    {
      std::cout << node->getInteger();
    }
    else if (node->getBaseType() == SPL_BASE_FLOAT)
    {
      std::cout << node->getFloat();
    }
    else if (node->getBaseType() == SPL_BASE_CHARACTERS)
    {
      std::cout << node->getCharacters();
    }
    else if (node->getBaseType() == SPL_BASE_POINTER)
    {
      std::cout << node->getPointer();
    }
    else
    {
      std::cout << "!!!broken!!!";
    }
    std::cout << std::endl;

    node = node->getNext();
    lexemeCount = lexemeCount + 1;
  }
}



} //namespace spc