//******************************************************************************
// Program: Simulation Programming Compiler (spc)
// Author: Matthew Nash
// Contributors:
//
// File: SplParser.cpp
// Description: Parser used in spc
// License: GPLv3
//
// This program is free software: you can redistribute it and/or modify it 
// under the terms of the GNU General Public License as published by the Free 
// Software Foundation, either version 3 of the License, or (at your option) 
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for 
// more details.
//
// You should have received a copy of the GNU General Public License along with 
// this program. If not, see https://www.gnu.org/licenses/. 
//******************************************************************************

#include "SplParser.h"

namespace spc
{


SplToken* matchExpression(SplToken* expression)
{
  SplToken* place = expression;
  // case: keyword statement
  if (place->tokenType == SPL_TOKEN_KEYWORD)
  {
    matchKeyword(place);
  }
  // case: might be a variable that already exists
  else if (place->tokenType == SPL_TOKEN_OPERATOR)
  {
    matchOperatorOneAry(place);
  }
  else if 
  (
    (place->next != NULL) &&
    (place->next->tokenType == SPL_TOKEN_OPERATOR)
  )
  {
    matchOperatorTwoAry(place);
  } 
  else
  {
    //TODO: compiler implementation error, see if we need more cases
  }
  
}

SplToken* matchKeyword(SplToken* expression)
{
  SplToken* place = expression;
  if (place->tokenType == SPL_TOKEN_KEYWORD)
  {
    matchKeyword(place);
  }
  if (place->name == "VARIABLE")
  {
    return matchVariable(place);
  }
  else if (place->name == "FUNCTION")
  {
    return matchFunction(place);
  }
  else if (place->name == "INPUT")
  {
    return matchInput(place);
  }
  else if (place->name == "ACTION")
  {
    return matchAction(place);
  }
  else if (place->name == "OUTPUT")
  {
    return matchOutput(place);
  }
  else if (place->name == "IF")
  {
    return matchIf(place);
  }
  else if (place->name == "THEN")
  {
    return matchIf(place);
  }
  else if (place->name == "WHILE")
  {
    return matchWhile(place);
  }
  else if (place->name == "UNTIL")
  {
    return matchUntil(place);
  }
  else
  {
    //TODO: add compiler implementation error
  }
}

//VARIABLE FLOAT FIRST_VARIABLE : 50.0 '
//VARIABLE FLOAT FIRST_VARIABLE : EXPRESSION '
SplToken* matchVariable(SplToken* expression)
{
  SplToken* place = expression;
  SplToken* type = place->getNext();
  SplToken* name = type->getNext();
  SplToken* assignment = name->getNext();
  SplToken* valueExpression = assignment->getNext();

  if (checkSymbolDictionary(*name))
  {
    //TODO: add syntax error
  }

  if (valueExpression->baseType != type->baseType)
  {
    //TODO: add syntax error
  }

  if (name->tokenType != SPL_TOKEN_SYMBOL)
  {
    //TODO: add syntax error
  }

  // case: variable name doesn't exist, simple literal assignment
  SplToken newSymbol;
  if (valueExpression->tokenType == SPL_TOKEN_LITERAL)
  {
    newSymbol = *valueExpression;
    newSymbol.name = name->characters;
    newSymbol.tokenType = SPL_TOKEN_SYMBOL;

    place = valueExpression->next;
  }
  // case: need to find out what the expression beyond assignment is
  else
  {
    place = matchExpression(valueExpression);

    newSymbol = SPL_PARSER_STACK->pop();
    newSymbol.name = name->characters;
    newSymbol.tokenType = SPL_TOKEN_SYMBOL;
    
  }

  SPL_SYMBOL_DICTIONARY->insert(newSymbol);

  // TODO: determine if we need to push symbol everytime we make one?
  //SPL_PARSER_STACK->push(newSymbol);

  return place;
}

SplToken* matchFunction(SplToken* expression)
{
  //TODO: throw not implemented
}

SplToken* matchInput(SplToken* expression)
{
  //TODO: throw not implemented
}

SplToken* matchAction(SplToken* expression)
{
  //TODO: throw not implemented
}

SplToken* matchOutput(SplToken* expression)
{
  //TODO: throw not implemented
}

SplToken* matchIf(SplToken* expression)
{
  //TODO: throw not implemented
}

SplToken* matchWhile(SplToken* expression)
{
  //TODO: throw not implemented
}

SplToken* matchUntil(SplToken* expression)
{
  //TODO: throw not implemented
}

// OPERATION LITERAL_OR_SYMBOL
// OPERATION EXPRESSION
SplToken* matchOperatorOneAry(SplToken* expression)
{
  SplToken* place = expression;
  SplToken* op = expression;
  SplToken* valueExpression = op->next;

  if (valueExpression == NULL) 
  {
    // TODO: make syntax error
  }

  // case: argument is a literal or a symbol
  if 
  (
    (valueExpression->tokenType == SPL_TOKEN_LITERAL) ||
    (valueExpression->tokenType == SPL_TOKEN_SYMBOL)
  )
  {
    SPL_PARSER_STACK->push(*valueExpression);
    place = valueExpression->next;
  }
  // case: argument is an expression that needs to be evaluated further
  else 
  {
    place = matchExpression(valueExpression);

    //we assume whatever value we needed got added to the stack
  }

  SplToken toCheck = SPL_PARSER_STACK->peek();

  // case: the stack is empty, something went wrong
  if 
  (
    (toCheck.tokenType == SPL_TOKEN_UNKNOWN) &&
    (toCheck.name == "WARNING - STACK EMPTY")
  )
  {
    // TODO: add syntax error, stack empty
  }

  // TODO: see if wee can do this with no ambiguity
  /*
  if 
  (
    (toCheck.baseType != SPL_BASE_INTEGER) ||
    (toCheck.baseType != SPL_BASE_FLOAT)
  )
  {
    // TODO: add syntax error, argument type is bad
  }
  */

  SPL_PARSER_STACK->push(*op);

  return place;
}


// ARG1_SYMBOL_OR_LITERAL OPERATION ARG2_SYMBOL_OR_LITERAL
// ARG1_SYMBOL_OR_LITERAL OPERATION EXPRESSION
SplToken* matchOperatorTwoAry(SplToken* expression)
{
  SplToken* place = expression;

  SplToken* arg1 = expression;
  SplToken* op = arg1->next;
  SplToken* arg2 = op->next;

  if 
  (
    (arg1->tokenType != SPL_TOKEN_SYMBOL) ||
    (arg1->tokenType != SPL_TOKEN_LITERAL)
  )
  {
    //TODO: add syntax error, first argument is not right token type
  }

  if (arg2 == NULL)
  {
    //TODO: add syntax error, second argument doesn't exist
  }

  //case: second argument is a simple literal
  if
  (
    (arg2->tokenType == SPL_TOKEN_LITERAL) ||
    (arg2->tokenType == SPL_TOKEN_SYMBOL)
  )
  {
    if (arg1->baseType != arg2->baseType)
    {
      // TODO: add syntax type error
    }

    SPL_PARSER_STACK->push(*arg2);

    place = arg2->next;

  }
  //case: second argument is an expression
  else
  {
    place = matchExpression(arg2);

    // we assume the argument we need was pushed to the stack

    // TODO: figure out if we can do this with no ambiguity
    /*
    if (arg1->baseType != SPL_PARSER_STACK->peek().baseType)
    {
      // TODO: add syntax type error
    }
    */
  }

  SPL_PARSER_STACK->push(*arg1);
  SPL_PARSER_STACK->push(*op);

  return place;
}

void parse()
{
  SplToken* place = SPL_LEXEMES_LIST_BEGIN;
  while (place != NULL)
  {
    place = matchExpression(place);
  }
}

}