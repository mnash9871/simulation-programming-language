//******************************************************************************
// Program: Simulation Programming Compiler (spc)
// Author: Matthew Nash
// Contributors:
//
// File: SplParser.h
// Description: Parser used in spc
// License: GPLv3
//
// This program is free software: you can redistribute it and/or modify it 
// under the terms of the GNU General Public License as published by the Free 
// Software Foundation, either version 3 of the License, or (at your option) 
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for 
// more details.
//
// You should have received a copy of the GNU General Public License along with 
// this program. If not, see https://www.gnu.org/licenses/. 
//******************************************************************************

#ifndef SPL_LEXER
#define SPL_LEXER

#include "SplGlobals.h"
#include "stdio.h"
#include <fstream>
#include <iostream>
#include <cassert>

namespace spc
{

// top level match function
SplToken* matchExpression(SplToken* expression);

// keyword matching rules
SplToken* matchKeyword(SplToken* expression);
SplToken* matchVariable(SplToken* expression);
SplToken* matchFunction(SplToken* expression);
SplToken* matchInput(SplToken* expression);
SplToken* matchAction(SplToken* expression);
SplToken* matchOutput(SplToken* expression);
SplToken* matchIf(SplToken* expression);
SplToken* matchWhile(SplToken* expression);
SplToken* matchUntil(SplToken* expression);

// 1-ary look ahead matching rules
SplToken* matchOperatorOneAry(SplToken* expression);
SplToken* matchNegate(SplToken* expression);
SplToken* matchBinaryNegate(SplToken* expression);
SplToken* matchLogicNot(SplToken* expression);

// 2-ary look ahead matching rules
SplToken* matchOperatorTwoAry(SplToken* expression);

// helper functions
void createOperator(SplToken* newOperator);
void createVariable(SplToken* newVariable);
void createLiteral(SplToken* newLiteral);
void stackify(); //turns infix into postfix

// worker functions
void parse();

}
#endif