//******************************************************************************
// Program: Simulation Programming Compiler (spc)
// Author: Matthew Nash
// Contributors:
//
// File: SplStack.h
// Description: Stack to make postfix intermediate code
// License: GPLv3
//
// This program is free software: you can redistribute it and/or modify it 
// under the terms of the GNU General Public License as published by the Free 
// Software Foundation, either version 3 of the License, or (at your option) 
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for 
// more details.
//
// You should have received a copy of the GNU General Public License along with 
// this program. If not, see https://www.gnu.org/licenses/. 
//******************************************************************************

#ifndef SPL_STACK
#define SPL_STACK

#include "SplNode.h"

namespace spc
{

class SplStack
{
public:
  SplStack();
  ~SplStack();

  SPL_INTEGER push(SplToken& pushInput);
  SplToken pop();
  SplToken pop
  (
    //output
    SPL_INTEGER& newHeight
  );
  SplToken peek();
  SplToken peek
  (
    //output
    SPL_INTEGER& heightOutput
  );
  SPL_INTEGER getHeight();

private:

  SplNode* _top;
  SPL_INTEGER _height;

};

} //namespace spc

#endif