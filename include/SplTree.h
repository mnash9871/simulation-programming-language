//******************************************************************************
// Program: Simulation Programming Compiler (spc)
// Author: Matthew Nash
// Contributors:
//
// File: SplTree.h
// Description: Collective data structure used in spc
// License: GPLv3
//
// This program is free software: you can redistribute it and/or modify it 
// under the terms of the GNU General Public License as published by the Free 
// Software Foundation, either version 3 of the License, or (at your option) 
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for 
// more details.
//
// You should have received a copy of the GNU General Public License along with 
// this program. If not, see https://www.gnu.org/licenses/. 
//******************************************************************************

#ifndef SPL_TREE
#define SPL_TREE

#include "SplNode.h"

namespace spc
{

class SplTree
{
public:
  SplTree();
  ~SplTree();


  void insert(SplToken newToken);
  SplToken getToken(SPL_CHARACTERS tokenName);
  bool contains(SPL_CHARACTERS tokenName);
  void remove(SPL_CHARACTERS tokenName);
  bool isEmpty();

private:
  SplNode* _top;
  SPL_INTEGER _height;

  //helper functions
  SPL_INTEGER getHeight(SplNode*);
  SPL_INTEGER maxHeight(SplNode* a, SplNode* b);
  SPL_INTEGER compare(SplNode* a, SplNode* b); //negative=a, positive=b, 0=equal

  SplNode* rotateRight(SplNode* y);
  SplNode* rotateLeft(SplNode* x);
  SPL_INTEGER getBalance(SplNode* n);
  SplNode* place(SplNode* node, SplToken* token);
  SplNode* find(SplNode* top, SPL_CHARACTERS tokenName);
  SplNode* removeNode(SplNode* top, SPL_CHARACTERS tokenName);

  void printTree(SplNode* top, SPL_INTEGER level);

};

} //namespace spc

#endif