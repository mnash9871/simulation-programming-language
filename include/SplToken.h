//******************************************************************************
// Program: Simulation Programming Compiler (spc)
// Author: Matthew Nash
// Contributors:
//
// File: SplToken.h
// Description: Token data structure used in spc
// License: GPLv3
//
// This program is free software: you can redistribute it and/or modify it 
// under the terms of the GNU General Public License as published by the Free 
// Software Foundation, either version 3 of the License, or (at your option) 
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for 
// more details.
//
// You should have received a copy of the GNU General Public License along with 
// this program. If not, see https://www.gnu.org/licenses/. 
//******************************************************************************
#ifndef SPL_TOKEN
#define SPL_TOKEN

#include <string>
#include <cassert>

namespace spc
{

#define SPL_POINTER void* 
#define SPL_INTEGER long long int
#define SPL_FLOAT double
#define SPL_CHARACTERS std::string


enum SPL_BASE_TYPES
{
  SPL_BASE_POINTER = 0,
  SPL_BASE_INTEGER = 1,
  SPL_BASE_FLOAT = 2,
  SPL_BASE_CHARACTERS = 3,
};

enum SPL_TOKEN_TYPES
{
  SPL_TOKEN_UNKNOWN = 0,
  SPL_TOKEN_LITERAL = 1,
  SPL_TOKEN_OPERATOR = 2,
  SPL_TOKEN_KEYWORD = 3,
  SPL_TOKEN_SYMBOL = 4
};

class SplToken
{
public:
  SplToken();
  SplToken(SPL_CHARACTERS newName);
  SplToken(SPL_CHARACTERS newName, SPL_INTEGER newValue);
  SplToken(SPL_CHARACTERS newName, SPL_FLOAT newValue);
  SplToken(SPL_CHARACTERS newName, SPL_CHARACTERS newValue);
  SplToken(SplToken&);
  //SplToken(SplToken&&);
  ~SplToken();

  //operator overload
  void operator=(const SplToken&);

  //metadata
  SPL_CHARACTERS name;
  SPL_BASE_TYPES baseType;
  SPL_TOKEN_TYPES tokenType;

  //data
  SPL_POINTER pointer;
  SPL_INTEGER integer;
  SPL_FLOAT floating;
  SPL_CHARACTERS characters;
  SplToken* next;

  //getters
  SPL_BASE_TYPES getBaseType();
  SPL_TOKEN_TYPES getTokenType();
  SPL_POINTER getPointer();
  SPL_INTEGER getInteger();
  SPL_FLOAT getFloat();
  SPL_CHARACTERS getCharacters();
  void getOperation(SPL_CHARACTERS& op, SPL_INTEGER& args);
  SplToken* getNext();

  //setters
  void setBaseType(SPL_BASE_TYPES newBaseType);
  void setTokenType(SPL_TOKEN_TYPES newTokenType);
  void setPointer(SPL_POINTER newValue);
  void setInteger(SPL_INTEGER newValue);
  void setFloat(SPL_FLOAT newValue);
  void setCharacters(SPL_CHARACTERS newValue);

  //helpers
  void clear();
  void clean();
  void nullify();

private:
  void initialize();
  void copy(const SplToken&);

};

}

#endif