//******************************************************************************
// Program: Simulation Programming Compiler (spc)
// Author: Matthew Nash
// Contributors:
//
// File: SplNode.h
// Description: Node data structure used in SplTree spc
// License: GPLv3
//
// This program is free software: you can redistribute it and/or modify it 
// under the terms of the GNU General Public License as published by the Free 
// Software Foundation, either version 3 of the License, or (at your option) 
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for 
// more details.
//
// You should have received a copy of the GNU General Public License along with 
// this program. If not, see https://www.gnu.org/licenses/. 
//******************************************************************************

#ifndef SPL_NODE
#define SPL_NODE

#include "SplToken.h"

namespace spc
{

class SplNode
{
public:
  SplNode();
  SplNode(SplToken&);
  SplNode(SplNode&);
  void operator=(const SplNode&);
  
  SplToken token;
  SplNode* left;
  SplNode* right;
  SPL_INTEGER height;
};


}

#endif