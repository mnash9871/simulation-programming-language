//******************************************************************************
// Program: Simulation Programming Compiler (spc)
// Author: Matthew Nash
// Contributors:
//
// File: SplGlobal.h
// Description: Items and functions needed by all parts of spc
// License: GPLv3
//
// This program is free software: you can redistribute it and/or modify it 
// under the terms of the GNU General Public License as published by the Free 
// Software Foundation, either version 3 of the License, or (at your option) 
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for 
// more details.
//
// You should have received a copy of the GNU General Public License along with 
// this program. If not, see https://www.gnu.org/licenses/. 
//******************************************************************************

#ifndef SPL_GLOBALS
#define SPL_GLOBALS

#include "SplTree.h"
#include "SplStack.h"
#include "stdio.h"
#include <fstream>
#include <iostream>

namespace spc
{

// non user defined lexemes
SplTree* SPL_WHITESPACE_DICTIONARY;
SplTree* SPL_OPERATOR_DICTIONARY;
SplTree* SPL_INTEGERS_DICTIONARY;
SplTree* SPL_KEYWORD_DICTIONARY;

// user defined lexemes
SplTree* SPL_SYMBOL_DICTIONARY;

// lexer final output
SplToken* SPL_LEXEMES_LIST_BEGIN;
SplToken* SPL_LEXEMES_LIST_END;

// parser intermediates
SplStack* SPL_PARSER_STACK;
SplToken* SPL_PARSED_LIST_BEGIN;
SplTree* SPL_PARSED_VARIABLES;
SplTree* SPL_PARSED_OPERATORS;

// init functions
void loadWhitespaceDictionary();
void loadOperatorDictionary();
void loadIntegersDictionary();
void loadKeywordDictionary();

// helper functions
bool checkWhitespaceDictionary(SplToken& toCheck);
bool checkOperatorDictionary(SplToken& toCheck);
bool checkIntegersDictionary(SplToken& toCheck);
bool checkKeywordDictionary(SplToken& toCheck);
bool checkSymbolDictionary(SplToken& toCheck);

void initialize();
void close();


}
#endif