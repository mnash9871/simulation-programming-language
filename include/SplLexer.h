//******************************************************************************
// Program: Simulation Programming Compiler (spc)
// Author: Matthew Nash
// Contributors:
//
// File: SplLexer.h
// Description: Lexical analyzer for spc
// License: GPLv3
//
// This program is free software: you can redistribute it and/or modify it 
// under the terms of the GNU General Public License as published by the Free 
// Software Foundation, either version 3 of the License, or (at your option) 
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for 
// more details.
//
// You should have received a copy of the GNU General Public License along with 
// this program. If not, see https://www.gnu.org/licenses/. 
//******************************************************************************

#ifndef SPL_LEXER
#define SPL_LEXER

#include "SplGlobals.h"
#include "stdio.h"
#include <fstream>
#include <iostream>

namespace spc
{

// lexeme being built
SplToken SPL_LEXEME;

// helper functions
void trimLexeme();

//worker functions
void lex
(
  //inputs
  SPL_CHARACTERS fileName
);
void printLexemes();

}
#endif